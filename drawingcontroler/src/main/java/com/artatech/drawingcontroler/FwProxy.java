package com.artatech.drawingcontroler;

import android.annotation.SuppressLint;
import android.os.IBinder;



@SuppressLint({"PrivateApi"})
/* renamed from: com.by.api.hw.FwProxy */
public class FwProxy {


    public static String getProp(String str) {
        return spGet(str);
    }

    public static IBinder smGetService(String str) {
        return m2684a(str);
    }

    public static String spGet(String str) {
        try {
            return (String) Class.forName("android.os.SystemProperties").getMethod("get", new Class[]{String.class}).invoke(null, new Object[]{str});
        } catch (Exception e) {

            return null;
        }
    }

    /* renamed from: a */
    private static IBinder m2684a(String str) {
        try {
            return (IBinder) Class.forName("android.os.ServiceManager").getMethod("getService", new Class[]{String.class}).invoke(null, new Object[]{str});
        } catch (Exception e) {

            return null;
        }
    }
}
