package com.artatech.drawingcontroler;

import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class B288 {

    //---add  by A-GAN 2015-12-21
    public static final int EINK_DISPLAY_MODE_INIT = 0x01;

    public static final int EINK_DISPLAY_MODE_DU_FULL = 0x02;

    public static final int EINK_DISPLAY_MODE_GC16_FULL = 0x04;

    public static final int EINK_DISPLAY_MODE_A2 = 0x10;

    public static final int EINK_DISPLAY_MODE_GC16_LOCAL = 0x84;

    public static final int EINK_DISPLAY_MODE_GL16 = 0x20;

    public static final int EINK_DISPLAY_MODE_DU_RECT = 0x402;

    public static final int EINK_DISPLAY_MODE_GC16_RECT = 0x404;

    public static final int EINK_DISPLAY_MODE_A2_RECT = 0X410;

    public static final int EINK_DISPLAY_MODE_GC16_LOCAL_RECT = 0x484;

    public static final int EINK_DISPLAY_MODE_GL16_RECT = 0x420;


    // for if pipeline mode then hw calulate the cropDrity
    public static final int EINK_DISPLAY_STRATEGY_ONE_FRAME = 0;

    public static final int EINK_DISPLAY_STRATEGY_MORE_FRAME_WITHOUT_FLIP = 1;

    public static final int EINK_DISPLAY_STRATEGY_MORE_FRAME_WITH_FLIP = 2;

    public static final int EINK_DISPLAY_STRATEGY_ALL_FLIP_WITHOUT_LAST = 3;

//	public static final int EINK_DISPLAY_STRATEGY_FORCE_PIPELINE = 4;


    // for if pipeline mode then soft calulate the cropDrity
    public static final int EINK_DISPLAY_STRATEGY_ONE_FRAME_SOFT_CROPDRITY = 4;

    public static final int EINK_DISPLAY_STRATEGY_MORE_FRAME_WITHOUT_FLIP_SOFT_CROPDRITY = 5;

    public static final int EINK_DISPLAY_STRATEGY_MORE_FRAME_WITH_FLIP_SOFT_CROPDRITY = 6;

    public static final int EINK_DISPLAY_STRATEGY_ALL_FLIP_WITHOUT_LAST_SOFT_CROPDRITY = 7;

//	public static final int EINK_DISPLAY_STRATEGY_FORCE_PIPELINE_SOFT_CROPDRITY = 9;


    // for special scene
    public static final int EINK_DISPLAY_STRATEGY_UPDAT_STATUBAR_GC16_RECT = 8;  /*gc 16 update the stadubar layer*/

    /*force skip  prpeline */
    public static final  int  EINK_DISPLAY_STRATEGY_FORCE_PIPELINE = 0x100;
    /*split frames tage*/
    public static final  int  EINK_DISPLAY_STRATEGY_SPLIT_FRAMES  = 0X200;
    public static void setFlipAllwithoutLast()
    {
        setEinkUpdateStrategy(EINK_DISPLAY_STRATEGY_ALL_FLIP_WITHOUT_LAST,EINK_DISPLAY_MODE_A2,EINK_DISPLAY_MODE_A2,EINK_DISPLAY_MODE_GC16_LOCAL);
    }

    public static void setOneFrame()
    {
        setEinkUpdateStrategy(EINK_DISPLAY_STRATEGY_ONE_FRAME,EINK_DISPLAY_MODE_GC16_LOCAL,EINK_DISPLAY_MODE_A2,EINK_DISPLAY_MODE_GC16_LOCAL);
    }

    public static void setMoreFrameWithoutFlip()
    {
        setEinkUpdateStrategy(EINK_DISPLAY_STRATEGY_MORE_FRAME_WITHOUT_FLIP,EINK_DISPLAY_MODE_GC16_LOCAL_RECT,EINK_DISPLAY_MODE_GC16_LOCAL_RECT,EINK_DISPLAY_MODE_GC16_LOCAL_RECT);
    }

    public static void setFullRefresh()
    {
        //   setEinkUpdateStrategy(B288.EINK_DISPLAY_STRATEGY_FORCE_PIPELINE,EINK_DISPLAY_MODE_GC16_LOCAL_RECT,EINK_DISPLAY_MODE_GC16_LOCAL_RECT,EINK_DISPLAY_MODE_GC16_LOCAL_RECT);
        setEinkUpadteMode(B288.EINK_DISPLAY_MODE_GC16_FULL);
    }

    public static void setA2()
    {
        setEinkUpadteMode(B288.EINK_DISPLAY_MODE_A2);
    }




    public static void refreshScreen(View view)
    {
        setEinkUpdateStrategy(EINK_DISPLAY_STRATEGY_ALL_FLIP_WITHOUT_LAST,
                EINK_DISPLAY_MODE_GC16_FULL,
                EINK_DISPLAY_MODE_GC16_FULL,
                EINK_DISPLAY_MODE_GC16_FULL);

        if(view!=null)
        {
        //    view.invalidate();

        }


        Handler handler = new Handler();
        handler.postDelayed(()->{setEinkUpdateStrategy(EINK_DISPLAY_STRATEGY_ONE_FRAME,
                EINK_DISPLAY_MODE_GC16_LOCAL,
                EINK_DISPLAY_MODE_GC16_LOCAL,
                EINK_DISPLAY_MODE_GC16_LOCAL);},500);


    }

    private static void setEinkUpdateStrategy(int strategy, int start_mode, int middle_mode, int 	end_mode) {
        try {
            Class<?> clazz = Class.forName("android.view.View");
            Method setEinkUodateModeMethod = clazz.getMethod("setEinkUpdateStrategy", int.class, int.class, int.class, int.class);

            setEinkUodateModeMethod.invoke(null, strategy, start_mode, middle_mode,end_mode);
            Log.i("result", "SET WORKED");
        } catch (Exception e) {
            e.printStackTrace();
            Log.i("result", "SET "+e.getLocalizedMessage());

        }
    }

    private static void setEinkUpadteMode(int mode)
    {
        try {
            Class<?> clazz = Class.forName("android.view.View");
            Method setEinkUodateModeMethod = clazz.getMethod("setEinkUpdateMode", int.class);

            setEinkUodateModeMethod.invoke(null, mode);
            Log.i("result", "SET WORKED");
        } catch (Exception e) {
            e.printStackTrace();
            Log.i("result", "SET "+e.getLocalizedMessage());

        }
    }

    public static int getEinkUpdateMode()
    {
        try {
            Class<?> clazz = Class.forName("android.view.View");
            Method getEinkUpdateMode = clazz.getMethod("getEinkUpdateMode");
            int result = (int) getEinkUpdateMode.invoke(null);
            return result;

        } catch (Exception e) {
            e.printStackTrace();
            return 0x11fc;
        }

    }

    public static int getEinkUpdateStrategy()
    {
        try {
            Class<?> clazz = Class.forName("android.view.View");
            Method getEinkUpdateMode = clazz.getMethod("getUpdateStrategy");
            int result = (int) getEinkUpdateMode.invoke(null);
            return result;

        } catch (Exception e) {
            e.printStackTrace();
            return 0x11fc;
        }
    }






    static int getIntValue(String fieldName)
    {
        int result = 0;
        try {
            result = (int) getFieldValue(fieldName);
        } catch (Exception e) {
            e.printStackTrace();
        }

        Log.i("result", fieldName+" = "+result);
        return result;
    }

    static Object getFieldValue(String fieldName) throws Exception {
        Class<?> clazz = Class.forName("android.view.View");
        Field myField = clazz.getDeclaredField(fieldName);
        return myField.get(null);
    }
}
