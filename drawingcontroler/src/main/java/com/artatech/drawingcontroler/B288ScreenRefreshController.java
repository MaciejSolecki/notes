package com.artatech.drawingcontroler;

import android.util.Log;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class B288ScreenRefreshController {

    public static final int EINK_DISPLAY_MODE_GC16_LOCAL = getIntValue("EINK_DISPLAY_MODE_GC16_LOCAL");
    public static final int EINK_DISPLAY_MODE_A2 = getIntValue("EINK_DISPLAY_MODE_A2");
    public static final int EINK_DISPLAY_STRATEGY_ALL_FLIP_WITHOUT_LAST = getIntValue("EINK_DISPLAY_STRATEGY_ALL_FLIP_WITHOUT_LAST");
    public static final int EINK_DISPLAY_STRATEGY_ONE_FRAME = getIntValue("EINK_DISPLAY_STRATEGY_ONE_FRAME");
    public static final int EINK_DISPLAY_MODE_GC16_LOCAL_RECT = getIntValue("EINK_DISPLAY_MODE_GC16_LOCAL_RECT");
    public static final int EINK_DISPLAY_MODE_A2_REC= getIntValue("EINK_DISPLAY_MODE_A2_REC");
    public static final int EINK_DISPLAY_STRATEGY_MORE_FRAME_WITHOUT_FLIP = getIntValue("EINK_DISPLAY_STRATEGY_MORE_FRAME_WITHOUT_FLIP");
    public static final int EINK_DISPLAY_MODE_DU_FULL = getIntValue("EINK_DISPLAY_MODE_DU_FULL");

    public static void setFlipAllwithoutLast()
    {
        setByEinkUpdateMode(EINK_DISPLAY_STRATEGY_ALL_FLIP_WITHOUT_LAST,EINK_DISPLAY_MODE_GC16_LOCAL,EINK_DISPLAY_MODE_GC16_LOCAL,EINK_DISPLAY_MODE_GC16_LOCAL);
    }

    public static void setOneFrame()
    {
        setByEinkUpdateMode(EINK_DISPLAY_STRATEGY_ONE_FRAME,EINK_DISPLAY_MODE_GC16_LOCAL,EINK_DISPLAY_MODE_GC16_LOCAL,EINK_DISPLAY_MODE_GC16_LOCAL);
    }

    public static void setMoreFrameWithoutFlip()
    {
        setByEinkUpdateMode(EINK_DISPLAY_STRATEGY_MORE_FRAME_WITHOUT_FLIP,EINK_DISPLAY_MODE_GC16_LOCAL_RECT,EINK_DISPLAY_MODE_GC16_LOCAL_RECT,EINK_DISPLAY_MODE_GC16_LOCAL_RECT);

    }

    public static void setFullRefresh()
    {
     //   setByEinkUpdateMode(B288.EINK_DISPLAY_STRATEGY_FORCE_PIPELINE,EINK_DISPLAY_MODE_GC16_LOCAL_RECT,EINK_DISPLAY_MODE_GC16_LOCAL_RECT,EINK_DISPLAY_MODE_GC16_LOCAL_RECT);
       setEinkUpadteMode(B288.EINK_DISPLAY_MODE_GC16_FULL);
    }

    public static void setA2()
    {
        setEinkUpadteMode(B288.EINK_DISPLAY_MODE_A2);
    }

    private static void setByEinkUpdateMode(int strategy, int start_mode,int middle_mode, int 	end_mode) {
        try {
            Class<?> clazz = Class.forName("android.view.View");
            Method setEinkUodateModeMethod = clazz.getMethod("setEinkUpdateStrategy", int.class, int.class, int.class, int.class);

            setEinkUodateModeMethod.invoke(null, strategy, start_mode, middle_mode,end_mode);
            Log.i("result", "SET WORKED");
        } catch (Exception e) {
            e.printStackTrace();
            Log.i("result", "SET "+e.getLocalizedMessage());

        }
    }

    private static void setEinkUpadteMode(int mode)
    {
        try {
            Class<?> clazz = Class.forName("android.view.View");
            Method setEinkUodateModeMethod = clazz.getMethod("setEinkUpdateMode", int.class);

            setEinkUodateModeMethod.invoke(null, mode);
            Log.i("result", "SET WORKED");
        } catch (Exception e) {
            e.printStackTrace();
            Log.i("result", "SET "+e.getLocalizedMessage());

        }
    }

    public static int getEinkUpdateMode()
    {
        try {
            Class<?> clazz = Class.forName("android.view.View");
            Method getEinkUpdateMode = clazz.getMethod("getEinkUpdateMode");
            int result = (int) getEinkUpdateMode.invoke(null);
            return result;

        } catch (Exception e) {
            e.printStackTrace();
            return 0x11fc;
        }

    }

    public static int getEinkUpdateStrategy()
    {
        try {
            Class<?> clazz = Class.forName("android.view.View");
            Method getEinkUpdateMode = clazz.getMethod("getUpdateStrategy");
            int result = (int) getEinkUpdateMode.invoke(null);
            return result;

        } catch (Exception e) {
            e.printStackTrace();
            return 0x11fc;
        }
    }






    static int getIntValue(String fieldName)
    {
        int result = 0;
        try {
            result = (int) getFieldValue(fieldName);
        } catch (Exception e) {
           e.printStackTrace();
        }

        Log.i("result", fieldName+" = "+result);
        return result;
    }

    static Object getFieldValue(String fieldName) throws Exception {
        Class<?> clazz = Class.forName("android.view.View");
        Field myField = clazz.getDeclaredField(fieldName);
        return myField.get(null);
    }
}
