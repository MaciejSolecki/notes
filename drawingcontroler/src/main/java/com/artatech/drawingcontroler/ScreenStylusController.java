package com.artatech.drawingcontroler;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import android.util.Log;

import java.lang.reflect.Method;
import java.util.ArrayList;

public class ScreenStylusController {


    public static final int ENABLE_FAST_DRAW_CODE = 4001;
    public static final int DISABLE_FAST_DRAW_CODE = 4000;
    public static final int GET_ENABLE_STATE_CODE = 4002;
    public static final int LOCK_FAST_DRAW_CODE = 4005;
    public static final int UNLOCK_FAST_DRAW_CODE = 4006;

    public static final int ADJUST_PEN_SIZE_CODE = 2003;
    public static final int MAX_PEN_SIZE = 100;
    public static final int MIN_PEN_SIZE=0;

    public static final int PRESURE_MODE_CODE = 2006;
    public static final int PRESURE_MODE_NORMAL= 0;
    public static final int PRESURE_MODE_DEFAULT = 1;
    public static final int PRESURE_MODE_PRESSURE = 2;



    public static final int CHANGE_COLOR_CODE = 2007;
    public static final int COLOR_BLACK =0;
    public static final int COLOR_WHITE =1;
    public static final int COLOR_TRANSPARENT =2;

    public static final int REMOVE_ALL_AREA_SETTINGS_CODE = 5006;

    public static final int ADD_DRAW_AREA_CODE = 7002;
    public static final int REMOVE_DRAW_AREA_CODE=7003;
    public static final int CLEAN_DRAW_AREA= 7006;


    private static ScreenStylusController instance;
    private int drawAreaIndex=1;
    private final IBinder binder;
    public ArrayList<Parcel> allResponses = new ArrayList<Parcel>();

    public static ScreenStylusController getInstance() {
            if(instance == null) initializeInstance();
            return instance;
    }
    private static void initializeInstance()
    {
        IBinder binder = getServiceBinder();
        instance = new ScreenStylusController(binder);
        instance.firstCall();
       // instance.removeAllAreaSettings();
    }
    public int firstCall()
    {
        Parcel return2 = transactCodeAndFlag(GET_ENABLE_STATE_CODE, IBinder.FIRST_CALL_TRANSACTION);
        return return2.readInt();
    }



    private void removeAllAreaSettings()
    {
       transactJustCode(REMOVE_ALL_AREA_SETTINGS_CODE);
    }

    private ScreenStylusController(IBinder binder) {
        this.binder = binder;
    }

    private static IBinder getServiceBinder() {
        try {
            Class localClass = Class.forName("android.os.ServiceManager");
            Method getService = localClass.getMethod("getService", new Class[]{String.class});
            if (getService != null) {
                Object result = getService.invoke(localClass, new Object[]{"by.hw"});
                if (result != null) {
                    IBinder binder = (IBinder) result;
                    return binder;
                }
            }
        }catch (Exception e)
        {
            throw new RuntimeException(e);
        }
        throw new RuntimeException("Service not found");
    }


    public void enableFastDraw()
    {
        transactJustCode(ENABLE_FAST_DRAW_CODE);
    }

    public void disableFstDraw()
    {
        transactJustCode(DISABLE_FAST_DRAW_CODE);
    }


    public boolean getFastDrawState()
    {
        Parcel reply = transactCodeAndFlag(GET_ENABLE_STATE_CODE, IBinder.FIRST_CALL_TRANSACTION);
        int tmp = reply.readInt();
        if(tmp>0) return true;
        return false;
    }
    private Parcel transactCodeAndFlag(int code, int transactionFlag)
    {
        try {
            Parcel reply = Parcel.obtain();
            Parcel data = Parcel.obtain();
            binder.transact(code, data, reply,transactionFlag);
           return reply;
        }
        catch (RemoteException e)
        {
            e.printStackTrace();
            return null;
        }
    }
    private void transactJustCode(int code){
   transactCodeAndFlag(code, IBinder.FIRST_CALL_TRANSACTION);
    }

    public void setPressureMode(int pressureMode)
    {
        int code = PRESURE_MODE_CODE;
        transactCodeAndData(code, pressureMode);
    }

   private void transactCodeAndData( int code, int data)
    {

        Parcel reply = Parcel.obtain();
        Parcel data1 = Parcel.obtain();

        data1.writeInt(data);
        try {
            binder.transact(code, data1, reply, IBinder.FIRST_CALL_TRANSACTION);
            Log.i("size","data sie : "+reply.dataSize());
            Log.i("size","data2 sie : "+data1.dataSize());

            allResponses.add(reply);
        } catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    private void transactCodeAndData( int code, Parcel data)
    {

        Parcel reply = Parcel.obtain();

        try {
            binder.transact(code, data, reply, IBinder.FIRST_CALL_TRANSACTION);
            Log.i("size","data sie : "+reply.dataSize());
            Log.i("size","data2 sie : "+data.dataSize());

            allResponses.add(reply);
        } catch (Exception e)
        {
            e.printStackTrace();
        }
    }


    public void addDrawArea(DrawArea drawArea)
    {
        transactCodeAndData(ADD_DRAW_AREA_CODE,drawArea.toParcal());
    }

    public void removeDrawArea(int index)
    {
        transactCodeAndData(REMOVE_DRAW_AREA_CODE, index);
    }

    public void removeAllDrawAreaSettings()
    {
        transactJustCode(REMOVE_ALL_AREA_SETTINGS_CODE);
    }


    public void cleanDrawArea(int drawAreaID)
    {
        transactCodeAndData(CLEAN_DRAW_AREA,drawAreaID);
    }
    @Override
    protected void finalize()
    {
        removeAllDrawAreaSettings();
      //  this.disableFstDraw();
    }


    public int getNextDrawAreaIndex() {
        return drawAreaIndex++;
    }
}
