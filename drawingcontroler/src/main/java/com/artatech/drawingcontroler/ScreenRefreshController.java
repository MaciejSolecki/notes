package com.artatech.drawingcontroler;

import android.view.View;

import java.lang.reflect.Method;

public class ScreenRefreshController {


    private static final int EPD_FULL = 1;
    private static final int EPD_A2 = 2;
    private static final int EPD_PART = 3;  //default mode
    private static final int EPD_BLACK_WHITE = 6;
    private static final int EPD_REGLA = 15;
    private static final int EPD_FAST = 18;

    private static final int EPD_DITHER = 100;
    private static final int EPD_FULL_DITHER = 102;
    private static ScreenRefreshController byRefreshRK3368;

    private int drawAreaIndex=1;

    private ScreenRefreshController() {

    }

    public int getNextDrawAreaIndex() {
        return drawAreaIndex++;
    }
    public static ScreenRefreshController getInstance() {
        if (byRefreshRK3368 == null) {
            synchronized (ScreenRefreshController.class) {
                if (byRefreshRK3368 == null)
                    byRefreshRK3368 = new ScreenRefreshController();
            }
        }
        return byRefreshRK3368;
    }

    private static void setByEinkUpdateMode(int updateMode) {
        try {
            Class<?> clazz = Class.forName("android.view.View");
            Method setEinkUodateModeMethod = clazz.getMethod("setEinkUpdateStrategy", int.class);
            setEinkUodateModeMethod.invoke(null, updateMode);
        } catch (Exception e) {
            e.printStackTrace();

        }
    }

    public static String getEinkUpdateMode() {
        try {
            Class<?> clazz = Class.forName("android.view.View");
            Method setEinkUodateModeMethod = clazz.getMethod("geByEinkUpdateMode");
          Object result = setEinkUodateModeMethod.invoke(null);
          return result.toString()+ "   "+result.getClass();
        } catch (Exception e) {
            e.printStackTrace();
            return "Exception    " +e.getLocalizedMessage();
        }
    }

    public void partRefresh() {
        setByEinkUpdateMode(EPD_PART);
    }

    public void a2Refresh() {
        setByEinkUpdateMode(EPD_A2);
    }

    public void fullRefresh() {
        setByEinkUpdateMode(EPD_FULL);
    }

    public void ditherRefresh() {
        setByEinkUpdateMode(EPD_DITHER);
    }

    public void regalRefresh() {
        setByEinkUpdateMode(EPD_REGLA);
    }

    public void blackWhiteRefresh() {
        setByEinkUpdateMode(EPD_BLACK_WHITE);
    }

    public void fastRefresh() {
        setByEinkUpdateMode(EPD_FAST);
    }

    public void fullDither() {
        setByEinkUpdateMode(EPD_FULL_DITHER);
    }


}
