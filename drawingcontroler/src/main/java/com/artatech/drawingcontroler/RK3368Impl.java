package com.artatech.drawingcontroler;

import android.view.View;

public class RK3368Impl  {
    private static final int EPD_A2 = 2;
    private static final int EPD_ADAPTIVE = 17;
    private static final int EPD_BLACK_WHITE = 6;
    private static final int EPD_DITHER = 100;
    public static final int EPD_FAST = 18;
    private static final int EPD_FORCE_FULL = 11;
    private static final int EPD_FULL = 1;
    public static final int EPD_FULL_DITHER = 102;
    public static final int EPD_PART = 3;
    private static final int EPD_REGLA = 15;

    /* renamed from: a */
    private static RK3368Impl f792a;

    private RK3368Impl() {
    }

    public static RK3368Impl getInstance() {
        if (f792a == null) {
            synchronized (RK3368Impl.class) {
                if (f792a == null) {
                    f792a = new RK3368Impl();
                }
            }
        }
        return f792a;
    }

    /* renamed from: a */
    private static void m740a(int i) {
        try {
            Class.forName("android.view.View").getMethod("setByEinkUpdateMode", new Class[]{Integer.TYPE}).invoke(null, new Object[]{Integer.valueOf(i)});
        } catch (Exception e) {
           e.printStackTrace();
        }
    }

    public void partRefresh() {
        m740a(3);
    }

    public void a2Refresh() {
        m740a(2);
    }

    public void fullRefresh() {
        m740a(1);
    }

    public void forceFullRefresh() {
        fullRefresh();
    }

    public void autoRefresh() {
        fastRefresh();
    }

    public void ditherRefresh() {
        m740a(100);
    }

    public void regalRefresh() {
        m740a(15);
    }

    public void defaultRefresh() {
        defaultRefresh(null);
    }

    public void defaultRefresh(View view) {
        if (FwProxy.getProp("ro.default.epdmode") != null && !FwProxy.getProp("ro.default.epdmode").equals("")) {
            m740a(Integer.valueOf(FwProxy.getProp("ro.default.epdmode")).intValue());
            if (view != null) {
                view.invalidate();
            }
        }
    }

    public void partRefresh(View view) {
        partRefresh();
    }

    public void a2Refresh(View view) {
        a2Refresh();
    }

    public void fullRefresh(View view) {
        fullRefresh();
    }

    public void autoRefresh(View view) {
        autoRefresh();
    }

    public void ditherRefresh(View view) {
        ditherRefresh();
    }

    public void regalRefresh(View view) {
        regalRefresh();
    }

    public void blackWhiteRefresh() {
        m740a(6);
    }

    public void adaptiveRefresh() {
        m740a(17);
    }

    public void fastRefresh() {
        m740a(18);
    }

    public void fullDither() {
        m740a(102);
    }
}
