package com.artatech.drawingcontroler;

import android.os.Parcel;
import android.util.Log;
import android.view.View;


/**
 *  The service understands vertical
 */
public class DrawArea {
    private final int index;
    private final int startx;
    private final int starty;
    private final int endx;
    private final int endy;


    public DrawArea(int index, int startx, int starty, int endx, int endy) {
        this.index = index;
        this.startx = startx;
        this.starty = starty;
        this.endx = endx;
        this.endy = endy;
    }

    public DrawArea(View view, int index)
    {
        this.index = index;

        int [] outLocation = new int[2];
        view.getLocationOnScreen(outLocation);

        this.endx = outLocation[1];
        int endy = outLocation[0];
        int starty = endy+view.getWidth();
        this.startx = this.endx +view.getHeight();
        Log.i("area", view.getId()+"");

        this.endy = 1404 - endy;
        this.starty = 1404-starty;

        Log.i("area","x1 : "+startx);
        Log.i("area","x2 : "+endx);
        Log.i("area","y1 : "+starty);
        Log.i("area","y2 : "+endy);
    }

    public int getIndex() {
        return index;
    }

    public Parcel toParcal()
    {
        Parcel parcel = Parcel.obtain();
        parcel.writeInt(index);
        parcel.writeInt(startx);
        parcel.writeInt(starty);
        parcel.writeInt(endx);
        parcel.writeInt(endy);

        return parcel;
    }
}
