package com.artatech.notes;

import android.content.Context;
import android.content.Intent;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;

import android.os.Environment;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;

import com.artatech.drawingcontroler.B288;
import com.artatech.drawingcontroler.B288ScreenRefreshController;
import com.artatech.drawingcontroler.DrawArea;
import com.artatech.drawingcontroler.GetDeviceModel;
import com.artatech.drawingcontroler.ScreenRefreshController;
import com.artatech.drawingcontroler.ScreenStylusController;
import com.artatech.notes.viewmodel.MainActivityViewModel;

import com.artatech.stylusview.Slate;

import java.io.File;

public class MainActivity extends AppCompatActivity {

    private Slate paintView;
    private Button ON;
    private TextView console;
    private String consoleText = "";
    private MainActivityViewModel mainActivityViewModel;


    private Button button3;





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main2);
        paintView = (Slate) findViewById(R.id.paintView);
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        ON = findViewById(R.id.ON);
        console = findViewById(R.id.textView);


        mainActivityViewModel = ViewModelProviders.of(this).get(MainActivityViewModel.class);
        button3 = findViewById(R.id.button3);

        //ScreenStylusController.getInstance().firstCall();
        //ScreenStylusController.getInstance().removeAllDrawAreaSettings();
        //ScreenStylusController.getInstance().enableFastDraw();
        //   ScreenStylusController.getInstance().enableFastDraw();
        //ScreenStylusController.getInstance().enableFastDraw();
        //ScreenStylusController.getInstance().removeDrawArea(0);
        //    ScreenStylusController.getInstance().addDrawArea(new DrawArea(paintView,1));
     //    ScreenStylusController.getInstance().enableFastDraw();
      //  startReaderSettings();
      //  startActivity(new Intent(this, FullscreenActivity.class));


     //   startActivity(new Intent(this, Main2Activity.class));

       // createDirectory();
      //  startReaderSettings();
        startActivity(new Intent(this,DischargeBattery.class));
    }

    String directory = Environment.getExternalStorageDirectory().getAbsolutePath()+"/ToBeDeletedFolder";


    public void startDischarge(View v)
    {
        startActivity(new Intent(this,DischargeBattery.class));

    }
    public void startReaderSettings()
    {
        Intent intent = new Intent();
        intent.setAction("com.artatech.android.intent.action.ACTION_READER_SETTINGS");
        startActivityForResult(intent,101);
    }

    public void removeDirectory(View view)
    {
        removeDirectory(directory,this);

        print("directory removed "+directory );
    }

    public void createDirectory(View view)
    {
        createDirectory(directory,this);
        print("directory creadted : "+directory);
    }

    public static void removeDirectory(String path, Context context)
    {
        File file = new File(path);
        file.delete();


        MediaScannerConnection.scanFile(context,new String[]{file.getAbsolutePath()},null,null);
        MediaScannerConnection.scanFile(context,new String[]{file.getParent()},null,null);

    }


    public void createDirectory()
    {

        createDirectory(Environment.getExternalStorageDirectory().getAbsolutePath(),"12HHHOLDMETHOD",this);

        String directory = Environment.getExternalStorageDirectory().getAbsolutePath()+"/12HHHNEWMETHOD";
        createDirectory(directory,this);
//       String directory =  Environment.getExternalStorageDirectory().getAbsolutePath();
//
//       directory +="/BBBBAAAAAfileOneOne232/fileTwo";
//        File file = new File(directory);
//        file.mkdirs();
//
//
//
//        MediaScannerConnection.scanFile(this,  new String[]{directory}, null, null);
//        MediaScannerConnection.scanFile(this, new String[]{(Environment.getExternalStorageDirectory().getAbsolutePath())},null,null);
//
//
//        new Handler(getMainLooper()).postDelayed(()->{
//            file.delete();
//            MediaScannerConnection.scanFile(this,  new String[]{file.getAbsolutePath()}, null, null);
//
//        },5000);

    }



    public static boolean createDirectory(String directoryPath, Context context)
    {
        File file = new File(directoryPath);
        String folderPath = file.getParentFile().getAbsolutePath();
        String name = file.getName();


        return createDirectory(folderPath, name, context);
    }


    public static boolean  createDirectory(String path, String directoryName, Context context)
    {




        String directory = path+"/"+directoryName+"/tempFIle";
        File file = new File(directory);
        file.mkdirs();



        MediaScannerConnection.scanFile(context,  new String[]{directory}, null, (String temporaryFilePath, Uri uri)->{removeFileAfterScan(temporaryFilePath,context);});
        MediaScannerConnection.scanFile(context, new String[]{(path)},null,null);




//        new Handler(getMainLooper()).postDelayed(()->{
//            file.delete();
//            MediaScannerConnection.scanFile(context,  new String[]{file.getAbsolutePath()}, null, null);
//
//        },5000);

        return true;
    }


    private static void removeFileAfterScan(String path, Context context)
    {
        File file = new File(path);
        file.delete();
        MediaScannerConnection.scanFile(context,  new String[]{file.getAbsolutePath()}, null, null);
    }


    public void print(String line)
    {
        consoleText = line +"\n"+consoleText;
        console.setText(consoleText);

    }

    public void getEingUpdate()
    {

       // print(ScreenRefreshController.getEinkUpdateMode());
    }


    int p=0;
    public void chngePressure(View v)
    {
        p++;


     //   button3.animate().translationX(500);
        if(p%2==1) {
            button3.animate().translationXBy(300).setDuration(5000);
        }
        else {
            button3.animate().setDuration(5000).translationXBy(-300);
        }

     //   ScreenStylusController.getInstance().setPressureMode(p);
        print("pressure : "+p);
    }
    boolean fast = false;

    public void bum() {
        fast=!fast;
        if(fast)
        {
           ScreenStylusController.getInstance().enableFastDraw();
            ScreenStylusController.getInstance().addDrawArea(new DrawArea(paintView,1));


            ScreenStylusController.getInstance().addDrawArea(new DrawArea(findViewById(R.id.imageView),2));
            print("fast");
           // ScreenStylusController.getInstance().addDrawArea(new DrawArea(paintView,2));
            ON.setText("OFF");
        }
        else
        {
            ScreenStylusController.getInstance().disableFstDraw();
//            ScreenStylusController.getInstance().removeAllDrawAreaSettings();

            ON.setText("ON");
        }
    }
    public void onClick(View view){

        if(GetDeviceModel.getModel().equals(GetDeviceModel.B288))
        {
            getUpdateMode();
            return;
        }

     try {
         bum();
       //  ScreenStylusController.getInstance().firstCall();


     }
     catch (Exception e)
     {
         e.printStackTrace();
         print(e.getLocalizedMessage());
     }


//     View view1 = findViewById(R.id.imageView);
//     new DrawArea(view1,2);
//     new DrawArea(findViewById(R.id.layout),3);
  //   ScreenStylusController.getInstance().addDrawArea(new DrawArea(paintView,1));

     // ScreenStylusController.getInstance().removeAllDrawAreaSettings();
    }







    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        switch (GetDeviceModel.getModel())
        {
            case GetDeviceModel.B288:
                menuInflater.inflate(R.menu.b288menu, menu);
                break;
            case GetDeviceModel.RK3368:
                menuInflater.inflate(R.menu.refresz, menu);
                break;
        }

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (GetDeviceModel.getModel())
        {
            case GetDeviceModel.B288:
                return onB288MenuItemSelected(item);
            case GetDeviceModel.RK3368:
                return onLargeReader(item);
        }
        return false;
    }
    public boolean onLargeReader(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.partRefresh:
                ScreenRefreshController.getInstance().partRefresh();
                break;
            case R.id.a2Refresh:
                ScreenRefreshController.getInstance().a2Refresh();
                break;
            case R.id.fullRefresh:
                ScreenRefreshController.getInstance().fullRefresh();
                break;
            case R.id.ditherRefresh:
                ScreenRefreshController.getInstance().ditherRefresh();
                break;
            case R.id.regalRefresh:
                ScreenRefreshController.getInstance().regalRefresh();
                break;
            case R.id.fastRefresh:
                ScreenRefreshController.getInstance().fastRefresh();
                break;
            case R.id.fullDither:
                ScreenRefreshController.getInstance().fullDither();
                break;
            case R.id.blackWhiteRefresh:
                ScreenRefreshController.getInstance().blackWhiteRefresh();
                break;
            case R.id.clear:
               paintView.clear();
                clear(item.getActionView());
                return true;
        }
        getEingUpdate();
        return super.onOptionsItemSelected(item);
    }

    public boolean onB288MenuItemSelected(MenuItem menuItem)
    {
        switch (menuItem.getItemId())
        {
            case R.id.a:
                     B288.setFullRefresh();
                return true;
            case R.id.b:
                B288ScreenRefreshController.setMoreFrameWithoutFlip();
                return true;
            case R.id.c:
                B288ScreenRefreshController.setOneFrame();
                return true;
            case R.id.d:
                B288ScreenRefreshController.setFlipAllwithoutLast();
                return true;
            case R.id.e:
                getUpdateMode();
                return true;
            case R.id.f:
                B288ScreenRefreshController.setA2();
                return true;
        }
        return false;
    }


    public void getUpdateMode()
    {


        print("");
        int result = B288ScreenRefreshController.getEinkUpdateMode();
        print("Eink update mode is : "+ Integer.toHexString(result));

        result = B288ScreenRefreshController.getEinkUpdateStrategy();
        print("Eink update strategy is : " +Integer.toHexString(result));
        print("Update MODE ::");
    }
    public void clear (View view)
    {
        //ScreenStylusController.getInstance()
        ScreenStylusController.getInstance().cleanDrawArea(2);
    }




}