package com.artatech.notes;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;

import java.util.Date;
import java.util.Random;
import java.util.StringJoiner;

public class DischargeBattery extends AppCompatActivity {


    TextView textView;
    Button button;
    ProgressBar progressBar;
    TextView timer;
    SeekBar seekBar;
    TextView targetLevel;

    private int targetBatteryLevel = 0;

    long time0 = 0;
    volatile  boolean isRunning = false;
    static int threadIndex=0;
    public static int getNext()
    {
        return threadIndex++;
    }

    public void addNewThread()
    {
        new Handler(Looper.getMainLooper()).postDelayed(()->{go(this);},100);
    }
    public  Thread go(final  DischargeBattery activity) {
        Thread go = new Thread()
        {
                private final long RUNNING_TIME =2 * 60 * 1000l;
                int index = getNext();
                Sha256 sha256 = new Sha256();
                public void run ()
                {
                    long startTime = new Date().getTime();
                    activity.print(index+"");
                    try {
                        int counter = 0;
                        byte[] random = new byte[64];
                        Random random1 = new Random();
                        random1.nextBytes(random);

                        while (isRunning&&running) {
                            long  timePassed = new Date().getTime()-startTime;
                            if (counter++ % 70000 == 0) {
                                activity.print("THread index :"+index + " SHA batch :  " + counter+"   thread running time : "+(timePassed/1000)+"s");
                            }
                            random = sha256.hash(random);

                            if(timePassed>RUNNING_TIME)
                            {
                                addNewThread();
                                break;
                            }
                        }
                    }catch (Exception e)
                    {
                        e.printStackTrace();
                        print(e.getLocalizedMessage());
                    }
                    print("Finished  "+index);
                }


        };

        go.start();
        return go;

    }
    public void run()
    {
        running=true;
        button.setText("stop");
        isRunning=true;
        progressBar.setVisibility(View.VISIBLE);
        print("Starat");
        time0 = new Date().getTime();
        timer.setText("0");
        for(int i=0;i<2;i++)
        {
            go(this);
        }

    }
    String console="";

    public void print(String text)
    {
        console = text+"\n"+ console;
       // textView.setText(console);

        textView.post(new Runnable() {
            @Override
            public void run() {
              textView.setText(console);
              long time = new Date().getTime() - time0;

              int batteryLavel = getBatteryPercentage(DischargeBattery.this);

              if(batteryLavel<=targetBatteryLevel)
              {
                  stop();
              }
                timer.setText(""+time+"ms" +"\nbattery = "+batteryLavel +"%");
            }

        });



    }



    public static int getBatteryPercentage(Context context) {

        if (Build.VERSION.SDK_INT >= 21) {

            BatteryManager bm = (BatteryManager) context.getSystemService(BATTERY_SERVICE);
            return bm.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY);

        } else {

            IntentFilter iFilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
            Intent batteryStatus = context.registerReceiver(null, iFilter);

            int level = batteryStatus != null ? batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL, -1) : -1;
            int scale = batteryStatus != null ? batteryStatus.getIntExtra(BatteryManager.EXTRA_SCALE, -1) : -1;

            double batteryPct = level / (double) scale;

            return (int) (batteryPct * 100);
        }
    }



    public void stop()
    {
        running=false;
        button.setText("start");
        progressBar.setVisibility(View.GONE);
        isRunning=false;


    }

    public static volatile  boolean running = false;
    public void startStop(View view){
        if(!running)
        {
            run();
        }
        else
        {
            stop();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_discharge_battery);

        timer = findViewById(R.id.textView3);
        button = findViewById(R.id.button);
        textView = findViewById(R.id.textView2);
        progressBar = findViewById(R.id.progressBar);
        targetLevel = findViewById(R.id.textView4);


        seekBar = findViewById(R.id.seekBar);
        targetLevel.setText("target battery level 0%");

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                targetBatteryLevel = progress;
                targetLevel.setText("target battery level = "+targetBatteryLevel+"%");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });


    }
}
