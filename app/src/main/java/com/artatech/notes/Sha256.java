package com.artatech.notes;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Sha256 {

    private MessageDigest messageDigest;


    private static final MessageDigest MESSAGE_DIGEST = getMessageDigest();

    public Sha256()
    {
        this.messageDigest = getMessageDigest();
    }

    private static final MessageDigest getMessageDigest()
    {
        try {
            return MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e.getMessage());
        }
    }


    public  byte [] hash(byte [] input)
    {
        return messageDigest.digest(input);
    }
}
