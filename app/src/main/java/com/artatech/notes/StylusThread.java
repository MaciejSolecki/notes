package com.artatech.notes;

import android.os.IBinder;

import com.artatech.drawingcontroler.ScreenStylusController;

public class StylusThread extends Thread{


    private final ScreenStylusController screenStylusController;
    public volatile boolean killThread=false;
    public StylusThread()
    {
        super();
        this.screenStylusController = ScreenStylusController.getInstance();
    }
    public void run()
    {
        while(true&& !killThread)
        {
           // screenStylusController.transactCodeAndFlag(ScreenStylusController.GET_ENABLE_STATE_CODE, IBinder.INTERFACE_TRANSACTION);
            try {
                sleep(16);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

}
