package com.artatech.notes;

import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.net.Uri;


public class GlobalReaderSettings {

    private static final String AUTHORITY = "com.artatech.android.inkbook.reader.settings.ReaderSettingsProvider";


    public static final String TIME_REFRESH_INTERVAL_KEY = "Time_Refresh_Interval";
    public static final String PAGE_SWITCH_REFRESH_INTERVAL_KEY = "Page_Switch_Refresh_Interval";


    public static final int GETTING_INDEX_VALUE = 571221;
    public static Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY);



    public static int setTimeRefreshInterval(ContentResolver resolver, int value)
    {
        return updateResolverValue(resolver, TIME_REFRESH_INTERVAL_KEY,value);
    }


    public static int setPageRefreshInterval(ContentResolver resolver, int value)
    {
       return updateResolverValue(resolver, PAGE_SWITCH_REFRESH_INTERVAL_KEY,value);
    }

    public static int getTimeRefreshInterval(ContentResolver resolver) {
        return updateResolverValue(resolver,TIME_REFRESH_INTERVAL_KEY,GETTING_INDEX_VALUE);
    }

    public static int getPageSwitchRefreshInterval(ContentResolver resolver) {
        return updateResolverValue(resolver,PAGE_SWITCH_REFRESH_INTERVAL_KEY,GETTING_INDEX_VALUE);
    }

    private static int updateResolverValue(ContentResolver resolver, String key, int value)
    {
        ContentValues updateValues = new ContentValues();
        updateValues.put(key, value);
        Uri SEND_URI = CONTENT_URI.buildUpon().appendEncodedPath(key).build();
        return resolver.update(SEND_URI, updateValues, null, null);
    }
}
