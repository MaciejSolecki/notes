package com.artatech.notes;

import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;

public class GlobalReaderSettingsTest extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_global_reader_settings_test);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


    }

    public void setTime(View view)
    {
        int result = GlobalReaderSettings.setTimeRefreshInterval(getContentResolver(),125);
    }

    public void setPage(View view)
    {
       int result =  GlobalReaderSettings.setPageRefreshInterval(getContentResolver(), 225);
    }

}
