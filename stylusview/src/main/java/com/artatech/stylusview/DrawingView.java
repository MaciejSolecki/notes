package com.artatech.stylusview;

import android.graphics.Bitmap;

public interface DrawingView {


    public void enableFastDraw();
    public void disableFastDraw();

}
